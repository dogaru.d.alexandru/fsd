import axios from 'axios';

export const instance = axios.create({
    baseURL: process.env.REACT_APP_API,
    timeout: 1000,
    headers: {'X-Custom-Header': ''}
  });
  