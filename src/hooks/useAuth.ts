import axios from "axios";
import { instance } from "../api/instance";

const login = (username:any, password:any) => {
    return axios
      .post( instance.getUri() + "signin", {
        username,
        password,
      })
      .then((response) => {
        if (response.data.accessToken) {
          localStorage.setItem("user", JSON.stringify(response.data));
        }
  
        return response.data;
      });
  };
  
  const logout = () => {
    localStorage.removeItem("user");
  };

export {login, logout}
