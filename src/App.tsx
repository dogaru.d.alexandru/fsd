import React, {createContext,useContext,useState} from "react";
import "./App.scss";

import Login from "./Pages/Login";
import MyTable from "./Pages/MyTable";
import Upload from "./Pages/Upload";
import Calendar from "./Components/Calendar";
// import DateSelector from './DateSelector';

import { Switch, BrowserRouter, Route } from "react-router-dom";
import {UserContext, Props,userStatus} from "./UserContext";

function App() {
  const [user,userState]=useState(userStatus.loggedOut);
  const values:Props={
    info:user,
    setInfo:userState
  }
  return (
    <BrowserRouter>
      <div className="App">
        <UserContext.Provider value={values}>
          <Switch>
            <Route path="/" exact component={Login} />
            <Route path="/table" component={MyTable} />
            <Route path="/upload" component={Upload} />
            <Route path="/calendar" component={Calendar} />
          </Switch>
        </UserContext.Provider>
      </div>
    </BrowserRouter>
  );
}

export default App;
