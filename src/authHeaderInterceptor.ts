import { instance } from "./api/instance";
export default function authHeader(){
    
    const item = localStorage.getItem('user')
    const user = item!=null? JSON.parse(item): null;

    if (user && user.authdata) {
        return { 'X-Custom-Header': user.accessToken};
    } else {
        return {};
    }
    
}