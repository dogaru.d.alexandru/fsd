import React from "react";

export const userStatus={
    loggedIn:true,
    loggedOut:false
};
export interface Props {
    info:boolean,
    setInfo: React.Dispatch<React.SetStateAction<boolean>> 
}
const UserContext = React.createContext({} as Props);

export  {UserContext};
