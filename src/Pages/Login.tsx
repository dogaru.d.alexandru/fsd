import React, { useContext } from "react";

import {
  Avatar,
  Box,
  Button,
  Card,
  Grid,
  Icon,
  InputAdornment,
  Link,
  Paper,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import { PersonPin, Email, Lock } from "@mui/icons-material";
import { UserContext, userStatus } from "../UserContext";
import { Link as RouterLink } from "react-router-dom";
import { color, width } from "@mui/system";
import { createTheme } from "@mui/material/styles";
import { green } from "@mui/material/colors";

const theme = createTheme({
  palette: {
    primary: {
      main: green[500],
    },
  },
});

const bgStyle = {
  background: "linear-gradient(45deg, #3c4ea8 45%, #c851be)",
  height: "100vh",
};

const Login = () => {
  const btnstyle = {
    margin: "8px 0",
    borderRadius: 25,
    width: "33vh",
    backgroundColor: "#4caf50",
  };
  const [text1, setText1] = React.useState("");
  const [text2, setText2] = React.useState("");
  const context = useContext(UserContext);
  const views = (text1: string, text2: string) => {
    if (text1 === "table" && text2 === "table") return "./table";
    if (text1 === "upload" && text2 === "upload") return "./upload";
    return "";
  };
  const setUserState = (text1: string, text2: string) => {
    if (views(text1, text2) === "./table" || views(text1, text2) === "./upload")
      context.setInfo(userStatus.loggedIn);
    else context.setInfo(userStatus.loggedOut);
  };
  return (
    <Grid style={bgStyle}>
      <Box style={{ position: "absolute", right: "0%", fontWeight: "bold" }}>
        {context.info === userStatus.loggedIn
          ? "User:LoggedIn"
          : "User:LoggedOut"}
      </Box>
      <Paper
        elevation={3}
        style={{
          position: "absolute",
          left: "50%",
          top: "50%",
          transform: "translate(-50%, -50%)",
          padding: 2,
          height: "60vh",
          width: "100vh",
          margin: "10px auto",
          borderRadius: 10,
        }}
      >
        <Grid container spacing={2} columns={2}>
          <Grid item xs={1}>
            <Box
              style={{
                position: "absolute",
                left: "25%",
                top: "50%",
                transform: "translate(-50%, -50%)",
              }}
            >
              <img src="head.png" height="250hv" />
            </Box>
          </Grid>

          <Grid
            item
            xs={2}
            style={{
              position: "absolute",
              left: "75%",
              top: "50%",
              transform: "translate(-50%, -50%)",
              width: "400hv",
            }}
          >
            <Stack
              direction="column"
              spacing={2}
              alignItems="flex-end"
              justifyContent="center"
              marginRight="20vh"
              width="60vh"
            >
              <Stack
                direction="row"
                alignItems="center"
                justifyContent="center"
                width="33vh"
              >
                <h2>User Login</h2>
              </Stack>

              <Box style={{ borderRadius: "25" }}>
                <TextField
                  onChange={(e) => setText1(e.target.value)}
                  type="text"
                  placeholder="Email Id"
                  variant="filled"
                  style={{ borderRadius: "25", width: "33vh" }}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        {
                          <Icon>
                            <Email
                              style={{
                                position: "absolute",
                                left: 13,
                                top: 15,
                                width: 25,
                                height: 25,
                              }}
                            />
                          </Icon>
                        }
                      </InputAdornment>
                    ),
                  }}
                />
              </Box>

              <TextField
                onChange={(e) => setText2(e.target.value)}
                placeholder="Password"
                type="password"
                variant="filled"
                style={{ borderRadius: 25, width: "33vh" }}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      {
                        <Icon>
                          <Lock
                            style={{
                              position: "absolute",
                              left: 13,
                              top: 15,
                              width: 25,
                              height: 25,
                            }}
                          />
                        </Icon>
                      }
                    </InputAdornment>
                  ),
                }}
              />

              <RouterLink to={views(text1, text2)}>
                <Button
                  type="submit"
                  variant="contained"
                  style={btnstyle}
                  onClick={() => {
                    setUserState(text1, text2);
                  }}
                >
                  Sign in
                </Button>
              </RouterLink>
              <Typography align="center" width="33vh">
                <Link href="#">Forgot username/password ?</Link>
              </Typography>
            </Stack>
          </Grid>
        </Grid>
      </Paper>
    </Grid>
  );
};

export default Login;
