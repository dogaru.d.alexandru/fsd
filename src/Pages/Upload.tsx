import { Stack, Button, Box, Paper } from "@mui/material";
import { UserContext, userStatus } from "../UserContext";
import { useContext } from "react";

export default function Upload() {
  const context = useContext(UserContext);
  return (
    <>
      <Box style={{ position: "absolute", right: "0%", fontWeight: "bold" }}>
        {context.info === userStatus.loggedIn
          ? "User:LoggedIn"
          : "User:LoggedOut"}
      </Box>
      <Box display="flex" justifyContent="center" alignItems="center">
        <Stack>
          <Paper style={{ backgroundColor: "#eceff1" }}>
            <Box mt={5} mb={3} mr={3} display="flex" justifyContent="flex-end">
              <Button
                style={{
                  backgroundColor: "#b0bec5",
                  color: "black",
                  width: 150,
                  textTransform: "none",
                  borderRadius: 0,
                }}
              >
                Upload
              </Button>
            </Box>
            <Box mx={3} mb={6}>
              <Paper elevation={0} style={{ borderRadius: 0, height: 400 }}>
                <img src="default-image.jpg" height={400} />
              </Paper>
            </Box>
          </Paper>
        </Stack>
      </Box>
    </>
  );
}
