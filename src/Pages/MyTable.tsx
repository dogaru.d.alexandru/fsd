import {
  Table,
  TableContainer,
  Paper,
  TableHead,
  TableCell,
  TableRow,
  TableBody,
  Box,
  Checkbox,
} from "@mui/material";
import { useContext } from "react";

import "../App.scss";
import { UserContext, userStatus } from "../UserContext";

function createData(
  imageName: string,
  size: number,
  recognitionResult: string,
  imageDownloadLink: string
) {
  return { imageName, size, recognitionResult, imageDownloadLink };
}

const rows = [
  createData("swfswdcwcwc", 45, "wecwewe", "wecwecwec"),
  createData("we", 25, "are", "the"),
  createData("champions", 31, "edwe", "Ameecwecwcwerica"),
  createData("qwrwefwewe", 65, "cwecwecwec", "cwecwe"),
  createData("cwecwecwecwe", 51, "wewcewec", "wecwecwecwe"),
];

export default function MyTable() {
  const context = useContext(UserContext);
  return (
    <Paper style={{ background: "#eeeeee", height: "100vh" }}>
      <Box style={{ position: "absolute", right: "0%", fontWeight: "bold" }}>
        {context.info === userStatus.loggedIn
          ? "User:LoggedIn"
          : "User:LoggedOut"}
      </Box>
      <Box
        style={{
          padding: "40px",
          paddingTop: "40px",
          paddingLeft: "50px",
          paddingRight: "50px",
          background: "#eeeeee",
        }}
      >
        <TableContainer component={Paper}>
          <Table aria-label="simple table" style={{ border: "1px solid " }}>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox />
                </TableCell>
                <TableCell style={{ fontWeight: "bold" }}>Image Name</TableCell>
                <TableCell style={{ fontWeight: "bold" }} align="right">
                  Size
                </TableCell>
                <TableCell style={{ fontWeight: "bold" }} align="right">
                  Recognition Result
                </TableCell>
                <TableCell style={{ fontWeight: "bold" }} align="right">
                  Image Download Link
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <TableRow key={row.imageName}>
                  <TableCell padding="checkbox">
                    <Checkbox />
                  </TableCell>
                  <TableCell component="th" scope="row">
                    {row.imageName}
                  </TableCell>
                  <TableCell align="right">{row.size}</TableCell>
                  <TableCell align="right">{row.recognitionResult}</TableCell>
                  <TableCell align="right">{row.imageDownloadLink}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Box>
    </Paper>
  );
}
