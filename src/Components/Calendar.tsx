import { Stack, Box, Button } from "@mui/material";
import {
  Table,
  TableContainer,
  Paper,
  TableHead,
  TableCell,
  TableRow,
  TableBody,
  Checkbox,
} from "@mui/material";
import { color } from "@mui/system";
import React, { useEffect, useState } from "react";
import { instance } from "../api/instance";
import axios from "axios";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import './calendarStyle.css';

interface Schedule {
  id: number;
  end_date: Date;
  start_date:Date;
  
}

export default function TableDatePicker() {
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const onChange = (dates: any) => {
    const [start, end] = dates;
    setStartDate(start);
    setEndDate(end);
  };

  
  //console.log("http://localhost:3000/schedules");
  const [schedules, setScheduleList] = useState<Schedule[]>([]);
  axios.get<Schedule[]>("http://localhost:3001/schedules")
        .then(response => {
            console.log(response.data);
            setScheduleList( response.data );
        });
  

  axios.post("http://localhost:3001/addedSchedule", {
    end_date: endDate,
  start_date: startDate
  })
  .then((response) => {
    console.log(response);
  }, (error) => {
    console.log(error);
  });
  
       
  return (
    <Box display="flex" justifyContent="center" alignItems="center">
      <Stack direction="column">
        <DatePicker
          selected={startDate}
          onChange={onChange}
          startDate={startDate}
          endDate={endDate}
          selectsRange

        >

          <Stack direction="row">
            <Button
              style={{
                margin: "8px 0",
                borderRadius: 25,
                width: "16.5vh",
                backgroundColor: "#F584D1",
                color: "white",
                position: "absolute",
                right: "0%",
                top: "100%",
              }}
            >
              Cancel
            </Button>
            <Button
              style={{
                margin: "8px 0",
                borderRadius: 25,
                width: "16.5vh",
                backgroundColor: "#F584D1",
                color: "white",
                position: "absolute",
                left: "0%",
                top: "100%",
              }}
            >
              Done
            </Button>
          </Stack>
        </DatePicker>
      </Stack>
      
    <Paper style={{ background: "#eeeeee", height: "100vh" }}>
     
      <Box
        style={{
          padding: "40px",
          paddingTop: "40px",
          paddingLeft: "50px",
          paddingRight: "50px",
          background: "#eeeeee",
        }}
      >
        <TableContainer component={Paper}>
          <Table aria-label="simple table" style={{ border: "1px solid " }}>
            <TableHead>
              <TableRow>
                <TableCell style={{ fontWeight: "bold" }}>Start Date</TableCell>
                <TableCell style={{ fontWeight: "bold" }} align="right">
                  End date
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {schedules.map((row) => (
                <TableRow key={row.id}>
                  <TableCell padding="checkbox">
                    <Checkbox />
                  </TableCell>
                  <TableCell component="th" scope="row">
                    {row.start_date}
                  </TableCell>
                  <TableCell align="right">{row.end_date}</TableCell>
                  
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Box>
    </Paper>

    </Box>
  );
}
// import { JsonCalendar } from "json-calendar";

// const calendar = new JsonCalendar();
// const month = calendar.weeks.map(w => w.map(d => d.day))
// //const obj = month[0][0]

// // const obj = calendar.weeks.map(w =>
// //   w.days.map(day => {
// //     const { className, id, day, date, monthIndex, year } = day
// //     // do something with the day's data
// //     return date.toLocaleString()
// //   })
// // )

// export {month};//{ obj };
