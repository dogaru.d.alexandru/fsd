import * as React from 'react';
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";

interface DateConstructor  {
    startDate: Date;
    endDate: Date;
} 

export default class DateSelector extends React.Component<{}, DateConstructor> {
    constructor(props:any) {
        super(props);
        this.state = {
            startDate: new Date(),
            endDate: new Date()
        };
        this.handleChange = this.handleChange.bind(this, this.state);
    }

    private handleChange(date:any, currentState:any) {
        if(currentState.startDate==null){
            console.log('Start date:', date);
            this.setState({
                startDate: date
            });
        }
        else if(currentState.endDate==null){
            console.log('End date:', date);
            this.setState({
                endDate: date
            });
        }
        else{
            currentState.startDate = null;
            currentState.endDate = null;
        }
    }

    public render() {
        const { startDate, endDate } = this.state;
        return (
            <DatePicker
                dateFormat="dd/MM/yyyy"
                selected={startDate} 
                startDate={startDate}
                endDate={endDate}
                onChange={this.handleChange}
                selectsRange
            />
        )
    }
}